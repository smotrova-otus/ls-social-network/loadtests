package ru.lilit.socialnetwork.logic

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import ru.lilit.socialnetwork.common

object Profiles {

  val getProfilesList: ChainBuilder = exec(
    http("Get profiles list")
      .get("/v0/profiles")
      .header("Authorization", "bearer ${token}")
      .queryParam("firstName", "${firstNameFilter}")
      .queryParam("lastName", "${lastNameFilter}")
      .queryParam("limit", 15)
      .queryParam("offset", 0)
      .check(status is 200)
  )

}
