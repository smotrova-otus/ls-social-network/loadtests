package ru.lilit.socialnetwork.logic

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import ru.lilit.socialnetwork.common

object Followings {

  val getFollowingList: ChainBuilder = exec(
    http("Get following list")
      .get(common.host + "/v0/profiles/${profileId}/followings")
      .header("Authorization", "bearer ${token}")
      .check(status is 200)
  )

}
