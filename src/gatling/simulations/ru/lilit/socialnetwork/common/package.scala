package ru.lilit.socialnetwork

import scala.util.Properties

package object common {

  val host = Properties.envOrElse("HOST", "https://smotrova-otus-hw3.scloud.dsxack.com")
  val users: Int = Integer.parseInt(Properties.envOrElse("USERS_COUNT", "100"))
  val duration: Int = Integer.parseInt(Properties.envOrElse("DURATION", "1"))

  val resourcesDir: String = "src/gatling/resources"

  val profiles: String = s"$resourcesDir/profiles.csv"

  val stage: Seq[String] = Properties.envOrElse("LOAD_PREPARE_STAGE", "").split(",")
  val profilesCount: Int = Integer.parseInt(Properties.envOrElse("PROFILES_COUNT", "10"))
}
