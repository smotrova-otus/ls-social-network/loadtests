package ru.lilit.socialnetwork.loadtests

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import ru.lilit.socialnetwork.common
import ru.lilit.socialnetwork.logic.{Followings, Profiles}

import scala.concurrent.duration._

class SearchSimulation extends Simulation {
  val feeder = csv("search.csv").random

  val scenarioBuilder: ScenarioBuilder = scenario("Get profiles list")
    .feed(feeder)
    .exec(
      Profiles.getProfilesList,
      Followings.getFollowingList
    )

  val httpProtocol = http
    .baseUrl(common.host)

  setUp(scenarioBuilder.inject(constantUsersPerSec(common.users) during (common.duration minutes))).protocols(httpProtocol)
}
