.PHONY: deploy
deploy:
	ansible-playbook -i ./deploy/inventory/hosts.yaml ./deploy/play-deploy.yaml -vv


.PHONY: reports
reports:
	ansible-playbook -i ./deploy/inventory/hosts.yaml ./deploy/play-reports.yaml -vv

